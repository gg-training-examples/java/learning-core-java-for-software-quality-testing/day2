package com.groundgurus.day2.exercises;

public class Cart {
	Item[] items = new Item[100];
	int count = 0;
	
	public void addToCart(Supermarket supermarket,
			String itemName) {
		items[count++] = supermarket.getItem(itemName);
	}
}
