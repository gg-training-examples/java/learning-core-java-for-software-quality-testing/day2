package com.groundgurus.day2.exercises;

public class Customer {
	String name;
	double cash;
	Cart cart;
	
	public Customer(String name, double cash) {
		this.name = name;
		this.cash = cash;
	}
}
