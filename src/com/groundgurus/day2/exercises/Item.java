package com.groundgurus.day2.exercises;

public class Item {
	String name;
	double price;
	int quantity;
	
	public Item(String name, double price, int quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
}
